const express = require("express");
require("express-async-errors");
const fs = require("fs");
const cors = require("cors");
const bodyParser = require("body-parser");
const allRoutes = require("./src/routes");
const dbConnect = require("./src/db");
const catchUnhandleExceptions = require("./src/middlewares/unhandle-exceptions");

const app = express();
app.use(cors({ origin: "*" }));
app.use(express.static("uploads"));
app.use(bodyParser.json());
app.use("/", allRoutes);
app.use(catchUnhandleExceptions);

async function bootstrarp() {
  try {
    if (!fs.existsSync("./uploads")) {
      console.log("upload dir not exists");
      fs.mkdirSync("./uploads");
    }

    await dbConnect();

    const PORT = process.env.PORT || 8000;
    app.listen(PORT, () => {
      console.log(`App is listening on port ${PORT} ..`);
    });
  } catch (err) {
    console.log("Error while connecting to db ..", err);
  }
}

bootstrarp();
