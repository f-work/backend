const express = require("express");
const jwt = require("jsonwebtoken");
const { OAuth2Client } = require("google-auth-library");
const suburbs = require("../data/SuburbsData.json");
const userService = require("../services/user");
const isAuth = require("../middlewares/is-auth");
const uploads = require("../utils/upload");
const sendMail = require("../utils/send-mail");

const router = express.Router();
const clientID =
  "672044993362-qaqf2o5d85sh59iia0ggmet7h9ilfpa3.apps.googleusercontent.com";
const client = new OAuth2Client(clientID);

router.get("/suburbs", async (req, res) => {
  res.send({ suburbs });
});

router.get("/profile/:id", async (req, res) => {
  const profile = await userService.singleUser(req.params.id);
  res.send({ profile });
});

router.post("/filter/suburbs", async (req, res) => {
  const suburb = req.body.suburb;

  const suburbs = await userService.filterSuburbs(suburb);
  res.send({ suburbs });
});

router.post("/google-login", async (req, res) => {
  console.log("body: ", req.body);

  const payload = await client.verifyIdToken({
    idToken: req.body.tokenId,
    audience: clientID,
  });

  const user = await userService.googleLogin(payload);
  const token = jwt.sign(
    { _id: user._id, email: user.email, name: user.name },
    "long-secret-key"
  );
  res.send({ user, token });
});

router.get("/profile", isAuth, async (req, res) => {
  const user = await userService.singleUser(req.user._id);
  res.send({ user });
});

router.patch("/profile", isAuth, uploads.single("image"), async (req, res) => {
  const userPayload = req.body;
  const userFields = {};
  for (const key in userPayload) {
    if (userPayload[key]) {
      userFields[key] = userPayload[key];
    }
  }

  if (req.file) {
    const url =
      req.protocol + "://" + req.headers.host + "/" + req.file.filename;
    userFields["drivingSchoolPic"] = url;
  }

  if (userFields.suburbs) {
    userFields["suburbs"] = userFields.suburbs.split(",");
  }
  console.log("fields: ", userFields);

  const user = await userService.updateProfile(req.user._id, userFields);
  res.send({ user });
});

router.post("/booking", async (req, res) => {
  const bookingFields = req.body;
  await sendMail(bookingFields);
  res.send({ message: "Successfully sent" });
});

module.exports = router;
