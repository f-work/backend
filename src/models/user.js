const mongoose = require("mongoose");

const userSchema = new mongoose.Schema({
  name: String,
  gender: String,
  drivingSchoolName: String,
  contactNo: String,
  email: String,
  pricePerHour: Number,
  transmission: String,
  serviceSuburbs: String,
  drivingSchoolPic: String,
  firstPackageHour: Number,
  firstPackagePrice: Number,
  secondPackageHour: Number,
  secondPackagePrice: Number,
  thirdPackageHour: String,
  thirdPackagePrice: String,
  suburbs: [String],
});

const User = mongoose.model("User", userSchema);
module.exports = User;
