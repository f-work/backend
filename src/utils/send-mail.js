const config = require("config");
const nodemailer = require("nodemailer");

const mailConfig = config.get("mail");
const transporter = nodemailer.createTransport({
  host: "gmail",
  service: "gmail",
  auth: {
    user: mailConfig.username,
    pass: mailConfig.password,
  },
});

module.exports = function ({ name, email, contactNo, message, toEmail }) {
  const text = `
        <p><b>Name</b>: ${name}</p>
        <p><b>Email</b>: ${email}</p>
        <p><b>Contact Number</b>: ${contactNo}</p>
        <p><b>Message</b>: ${message}</p>
    `;

  return transporter.sendMail({
    to: toEmail,
    subject: "Booking from " + email,
    html: text,
  });
};
