const User = require("../models/user");

async function googleLogin(userGooglePayload) {
  const { payload } = userGooglePayload;
  const { email, name, picture } = payload;
  await User.updateOne(
    { email },
    { email, name, drivingSchoolPic: picture },
    { upsert: true }
  );
  return await User.findOne({ email });
}

async function singleUser(userId) {
  return await User.findById(userId);
}

async function updateProfile(userId, userPayload) {
  return await User.findByIdAndUpdate(
    userId,
    { ...userPayload },
    { new: true }
  );
}

async function filterSuburbs(suburb) {
  if (suburb) {
    const actualSuburbVal = suburb.split(",")[0];

    // return await User.find({
    //   serviceSuburbs: { $regex: ".*" + actualSuburbVal + ".*" },
    // });

    const regex = new RegExp(".*" + actualSuburbVal + ".*");

    return await User.find({
      suburbs: regex,
    });
  }

  return await User.find();
}

module.exports = { googleLogin, singleUser, updateProfile, filterSuburbs };
